//
//  DuckShooterGameViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 05.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit
import SpriteKit

class DuckShooterGameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsDrawCount = true
        skView.showsNodeCount = true
        
        let scene = DuckShooterScene(size: view.bounds.size)
        skView.presentScene(scene)
        
        
    }
    
}
