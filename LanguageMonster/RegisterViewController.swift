//
//  RegisterViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 17.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var confirmPasswordTextField: UITextField!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var countrySelectionStackView: CustomStackView!
	
    var viewModel: RegisterViewModel!
	var scrollViewInsetCoordinator: ScrollViewKeyboardInsetCoordinator!
	var selectedCountry: CountryDTO?
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		scrollViewInsetCoordinator = ScrollViewKeyboardInsetCoordinator(scrollView: scrollView)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "PresentCountryPickerViewControllerSegue" {
			if let destinationVC = segue.destinationViewController as? UINavigationController {
				if let countryPickerVC = destinationVC.topViewController as? CountryPickerViewController {
					countryPickerVC.delegate = self
				}
			}
		}
	}
	
	// MARK: ui actions
	@IBAction func registerButtonTapped(sender: UIButton) {
		viewModel.email = emailTextField.text
		viewModel.password = passwordTextField.text
		viewModel.repeatPassword = confirmPasswordTextField.text
		viewModel.country = selectedCountry
		
		viewModel.registerUser().subscribeNext { registered in
			print(registered)
		}
	}
	
}

extension RegisterViewController: UITextFieldDelegate {
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		if let nextField = nextFieldForFieldWithTag(textField.tag) {
			nextField.becomeFirstResponder()
			return false
		}
		
		textField.resignFirstResponder()
		return true
	}
	
	private func nextFieldForFieldWithTag(tag: Int) -> UITextField? {
		let nextFieldTag = tag + 1
		return self.view.viewWithTag(nextFieldTag) as? UITextField
	}
}

extension RegisterViewController: CountryPickerViewControllerDelegate {
	func countryPickerDidSelectCountry(country: CountryDTO) {
		selectedCountry = country
		let text = "Selected country: \(selectedCountry?.name)"
		countrySelectionStackView.removeExistingLabel()
		countrySelectionStackView.addLabelWithText(text)
		dismissViewControllerAnimated(true, completion: nil)
	}
}