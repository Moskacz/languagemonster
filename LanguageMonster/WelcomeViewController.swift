//
//  WelcomeViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 06.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

}
