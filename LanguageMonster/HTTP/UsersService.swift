//
//  UsersService.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class UsersService {
	
	let httpClient: HttpClient
	
	init(httpClient: HttpClient) {
		self.httpClient = httpClient
	}
	
	func registerUserWithParameters(parameters: RegisterRequestParameters) -> Observable<Bool> {
		return deferred {
			let url = "http://language-monster.com/api/users"
			return self.httpClient.postResourceToURL(url, withParameters: parameters.dictionaryRepresentation, authorizationType: .DEVICE).map { response in
				debugPrint(response)
				return true
			}
		}
	}
	
	func loginUserWithParameters(parameters: LoginRequestParameters) -> Observable<User?> {
		return deferred {
			let url = "http://language-monster.com/api/users"
			return self.httpClient.putResourceToURL(url, withParameters: parameters.dictionaryRepresentation, authorizationType: .DEVICE).map { json in
				
				if let userJSON = DataDictionaryParser().jsonDictionaryFromObject(json) {
					return UserParser().userFromJSON(userJSON)
				}
				
				return nil
			}
		}
	}
	
}