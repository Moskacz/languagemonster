//
//  LanguagesService.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 18.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class LanguagesService {
    
	let httpClient: HttpClient
    
	init(httpClient: HttpClient) {
		self.httpClient = httpClient
	}
 
	convenience init() {
		self.init(httpClient: HttpClient())
	}

	func getCountries() -> Observable<[CountryDTO]> {
		return deferred {
			let url = "http://language-monster.com/api/languages"
			return self.httpClient.getResourceAtURL(url, withParameters: nil, authorizationType: .DEVICE).map { json in
				var countries = [CountryDTO]()
				
				if let jsonData = json as? [String: AnyObject] {
					if let data = jsonData["data"] as? Array<[String: AnyObject]> {
						let parser = CountryParser()
						for countryJSON in data {
							if let country = parser.countryFromJSON(countryJSON){
								countries.append(country)
							}
						}
					}
				}
				
				return countries
			}
		}
	}
	
	func getLanguagesPairs() -> Observable<[LanguagesPairDTO]> {
        return deferred {
            let url = "http://language-monster.com/api/languages/pairs"
            return self.httpClient.getResourceAtURL(url, withParameters: nil, authorizationType: .DEVICE).map { json in
                
                var languagesPairs = [LanguagesPairDTO]()
                
                if let jsonData = json as? [String: AnyObject] {
                    if let data = jsonData["data"] as? Array<[String: AnyObject]> {
                        let parser = LanguageParser()
                        for languagesPairJSON in data {
                            if let pair = parser.languagesPairFromJSON(languagesPairJSON) {
                                languagesPairs.append(pair)
                            }
                        }
                    }
                }
            
                return languagesPairs
            }
        }
	}
    
}