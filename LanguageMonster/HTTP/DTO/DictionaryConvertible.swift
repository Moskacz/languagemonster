//
//  DictionaryConvertible.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 17.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

protocol DictionaryConvertible {
    var dictionaryRepresentation: Dictionary<String, AnyObject> { get }
}