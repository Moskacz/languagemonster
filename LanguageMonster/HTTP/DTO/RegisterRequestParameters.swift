//
//  RegisterRequestParameters.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 17.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct RegisterRequestParameters {
	let email: String
	let password: String
	let repeatPassword: String
	let language: String
	let country: String
}

extension RegisterRequestParameters: DictionaryConvertible {
	
	var dictionaryRepresentation: Dictionary<String, AnyObject> {
		var dictionary = Dictionary<String, AnyObject>()
		
		dictionary["email"] = self.email
		dictionary["password1"] = self.password
		dictionary["password2"] = self.repeatPassword
		dictionary["language"] = self.language
		dictionary["country"] = self.country
		
		return dictionary
	}
	
}