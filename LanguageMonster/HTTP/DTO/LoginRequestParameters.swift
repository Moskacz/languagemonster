//
//  LoginRequestParameters.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 17.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct LoginRequestParameters {
	let email: String
	let password: String
}

extension LoginRequestParameters: DictionaryConvertible {
	
	var dictionaryRepresentation: Dictionary<String, AnyObject> {
		var dictionary = Dictionary<String, AnyObject>()
		
		dictionary["email"] = self.email
		dictionary["password"] = self.password
		
		return dictionary
	}
}