//
//  HttpClient.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 06.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

enum AuthorizationType {
	case API, DEVICE, USER
}

class HttpClient {
	
	func getResourceAtURL(urlString: String, withParameters parameters: [String: AnyObject]?, authorizationType: AuthorizationType) -> Observable<AnyObject?> {
		return create { observer in
			self.getAuthorizationHeadersForAuthorizationType(authorizationType).subscribeNext { authorizationHeaders in
				Alamofire.request(.GET, urlString, parameters: parameters, encoding: .URL, headers: authorizationHeaders).responseJSON { response in
					if let error = response.result.error {
						observer.onError(error)
					}
					
					if let json = response.result.value {
						observer.onNext(json)
						observer.onCompleted()
					}
				}
			}
		}
	}
	
	func postResourceToURL(urlString: String, withParameters parameters: [String: AnyObject]?, authorizationType: AuthorizationType) -> Observable<AnyObject?> {
		return create { observer in
			self.getAuthorizationHeadersForAuthorizationType(authorizationType).subscribeNext { authorizationHeaders in
				Alamofire.request(.POST, urlString, parameters: parameters, encoding: .JSON, headers: authorizationHeaders).responseJSON { response in
					if let error = response.result.error {
						observer.onError(error)
					}
					
					if let json = response.result.value {
						observer.onNext(json)
						observer.onCompleted()
					}
				}
			}
		}
	}
	
	func putResourceToURL(urlString: String, withParameters parameters: [String: AnyObject]?, authorizationType: AuthorizationType) -> Observable<AnyObject> {
		return create { observer in
			self.getAuthorizationHeadersForAuthorizationType(authorizationType).subscribeNext { authorizationHeaders in
				Alamofire.request(.PUT, urlString, parameters: parameters, encoding: .JSON, headers: authorizationHeaders).responseJSON { response in
					if let error = response.result.error {
						observer.onError(error)
					}
					
					if let json = response.result.value {
						observer.onNext(json)
						observer.onCompleted()
					}
				}
			}
		}
	}
	
}

extension HttpClient {
	
	private func getAuthorizationHeadersForAuthorizationType(authorizationType: AuthorizationType) -> Observable<[String: String]> {
		switch authorizationType {
		case .API:
			return just(self.authorizationHeaderWithToken("0Xb96z6FLvnHym48h5MDix5k4c0X5S1D"))
		case .DEVICE:
			return DeviceModel(devicesService: DevicesService(httpClient: self)).getDeviceToken().map { token in
				return self.authorizationHeaderWithToken(token)
			}
		case .USER:
			return just(["a":"b"])
		}
		
	}
	
	private func authorizationHeaderWithToken(token: String) -> [String: String] {
		return ["Authorization": "Token: \(token)"]
	}
	
}