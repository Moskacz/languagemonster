//
//  DevicesService.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 24.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class DevicesService {
    
	let httpClient: HttpClient
    
	init(httpClient: HttpClient) {
		self.httpClient = httpClient
	}
	
	func registerDevice() -> Observable<String> {
		return deferred({
			let url = "http://language-monster.com/api/devices"
			return self.httpClient.postResourceToURL(url, withParameters: nil, authorizationType: .API).map { response in
				if let json = response as? [String: AnyObject] {
					if let data = json["data"] as? [String: AnyObject] {
						if let deviceToken = data["device_key"] as? String {
							return deviceToken
						}
					}
				}
				
				return ""
			}
		})
	}
	
}