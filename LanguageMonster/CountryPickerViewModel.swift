//
//  CountryPickerViewModel.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 19.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class CountryPickerViewModel {
	
	let languagesModel: LanguagesModel
	var countries = [CountryDTO]()
	
    init(languagesModel: LanguagesModel) {
        self.languagesModel = languagesModel
    }
	
	func sectionsCount() -> Int {
		return 1
	}
    
	func itemsCountForSection(section: Int) -> Int {
		return countries.count
	}
	
	func countryForIndexPath(indexPath: NSIndexPath) -> CountryDTO? {
		return self.countries[indexPath.row]
	}
	
    func fetchCountries(completion: Void -> Void) {
		languagesModel.getAllCountries().subscribeNext { fetchedCountries in
			self.countries = fetchedCountries
            completion()
		}
	}
    
}
