//
//  AboutAppViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 17.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
