//
//  DashboardViewController.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var menuViewLeading: NSLayoutConstraint!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configureMenuButton()
        
        sideMenuView.hidden = true
        menuViewLeading.constant = -CGRectGetWidth(self.sideMenuView.bounds)
	}

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
	private func configureMenuButton() {
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu", style: .Plain, target: self, action: Selector("toggleSideMenu"))
	}
    
    private dynamic func toggleSideMenu() {
        if sideMenuView.hidden {
            showSideMenu()
        } else {
            hideSideMenu()
        }
    }
    
    private func hideSideMenu() {
        menuViewLeading.constant = -CGRectGetWidth(self.sideMenuView.bounds)
        layoutIfNeededWithCompletion {
            self.sideMenuView.hidden = true
        }
    }
    
    private func showSideMenu() {
        sideMenuView.hidden = false
        menuViewLeading.constant = 0.0
        layoutIfNeededWithCompletion{}
    }
    
    private func layoutIfNeededWithCompletion(completion: Void -> Void) {
        UIView.animateWithDuration(0.3, animations: {
            self.view.layoutIfNeeded()
        }) { completed in
            completion()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.SegueIdentifiers.EmbedMenuViewControlerSegue {
            let menuViewController = segue.destinationViewController as? MenuViewController
            menuViewController?.delegate = self
        }
    }
}

extension DashboardViewController: MenuViewControllerDelegate {
    func menuViewControllerDidSelectMenuItem(menuItem: MenuItem) {
        if menuItem == .Games {
            performSegueWithIdentifier(Constants.SegueIdentifiers.ShowGamesListViewControllerSegue, sender: self)
        }
        
        if menuItem == .Languages {
            performSegueWithIdentifier(Constants.SegueIdentifiers.ShowLanguagesPairsViewControllerSegue, sender: self)
        }
    }
}
