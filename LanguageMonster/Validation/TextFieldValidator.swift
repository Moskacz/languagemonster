//
//  TextFieldValidator.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 24.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class TextFieldValidator {
    
    weak var textField: UITextField?
    let validator: TextValidator
    
    init(textField: UITextField?, validator: TextValidator) {
        self.textField = textField
        self.validator = validator
        registerTextFieldEvents()
    }
    
    private func registerTextFieldEvents() {
        textField?.addTarget(self, action: "validateTextField", forControlEvents: .EditingDidEnd)
    }
    
    private dynamic func validateTextField() {
        if let text = textField?.text {
            textField?.backgroundColor = validator.isValid(text) ? UIColor.whiteColor() : UIColor.redColor()
        }
    }
}
