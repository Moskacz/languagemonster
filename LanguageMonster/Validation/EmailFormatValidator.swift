//
//  EmailFormatValidator.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 24.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class EmailFormatValidator: TextValidator {
    
    let emailRegex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", options: .CaseInsensitive)
    let allowedNumebrOfMatches = 1
    
    func isValid(text: String) -> Bool {
        let range = NSRange(location: 0, length: text.characters.count)
        return emailRegex.numberOfMatchesInString(text, options: .ReportCompletion, range: range) == 1
    }
    
}