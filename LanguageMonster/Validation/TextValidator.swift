//
//  Validator.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 24.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

protocol TextValidator {
    func isValid(text: String) -> Bool
}