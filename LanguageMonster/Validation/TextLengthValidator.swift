//
//  LengthValidator.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 24.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class TextLengthValidator: TextValidator {
    
    let requiredTextLength: Int
    
    init(requiredTextLength: Int) {
        self.requiredTextLength = requiredTextLength
    }
    
    func isValid(text: String) -> Bool {
        return text.characters.count >= requiredTextLength
    }
}