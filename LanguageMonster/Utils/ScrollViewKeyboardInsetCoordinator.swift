//
//  ScrollViewKeyboardInsetCoordinator.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 19.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class ScrollViewKeyboardInsetCoordinator {
    
    let notificationCenter = NSNotificationCenter.defaultCenter()
    weak var scrollView: UIScrollView?
    
    init(scrollView: UIScrollView?) {
        self.scrollView = scrollView
        registerForKeyboardNotifications()
    }
    
    // MARK: Notifications
    
    private func registerForKeyboardNotifications() {
        notificationCenter.addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: Selector("keyboardWillHide"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private dynamic func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as! [String: AnyObject]
        let keyboardSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
        scrollView?.contentInset.bottom = keyboardSize.height
        scrollView?.scrollIndicatorInsets.bottom = keyboardSize.height
    }
    
    private dynamic func keyboardWillHide() {
        scrollView?.contentInset.bottom = 0
        scrollView?.scrollIndicatorInsets.bottom = 0
    }
    
    // MARK: deinit
    
    deinit {
        notificationCenter.removeObserver(self)
    }
}