//
//  AutolayoutHelper.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 28.11.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class AutolayoutHelper {
    
    func matchingParentHorizontalConstraintsForView(view: UIView) -> [NSLayoutConstraint] {
        let views = ["view": view]
        return NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: .DirectionLeadingToTrailing, metrics: nil, views: views)
    }
    
    func matchingParentVerticalConstraintsForView(view: UIView) -> [NSLayoutConstraint] {
        let views = ["view": view]
        return NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: .DirectionLeadingToTrailing, metrics: nil, views: views)
    }
    
    func matchingParentConstraintsForView(view: UIView) -> [NSLayoutConstraint] {
        return matchingParentVerticalConstraintsForView(view) + matchingParentHorizontalConstraintsForView(view)
    }
    
    func heightConstraintForView(view: UIView, height: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: .None, attribute: .NotAnAttribute, multiplier: 0, constant: height)
    }
    
    func widthConstraintForView(view: UIView, width: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: view, attribute: .Width, relatedBy: .Equal, toItem: .None, attribute: .NotAnAttribute, multiplier: 0, constant: width)
    }
}