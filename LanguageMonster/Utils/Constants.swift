//
//  Constants.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct Constants {
	
	struct SegueIdentifiers {
		static let PresentCountryPickerViewControllerSegue = "PresentCountryPickerViewControllerSegue"
		static let ShowDashboardViewControllerSegue = "ShowDashboardViewControllerSegue"
		static let ShowMainViewControllerSegue = "ShowMainViewControllerSegue"
        static let ShowGamesListViewControllerSegue = "ShowGamesListViewControllerSegue"
        static let ShowLanguagesPairsViewControllerSegue = "ShowLanguagesPairsViewControllerSegue"
        static let EmbedMenuViewControlerSegue = "EmbedMenuViewControlerSegue"
	}
	
	struct CellIdentifiers {
		static let MenuItemTableViewCellIdentifier = "MenuItemTableViewCell"
        static let GameCollectionViewCell = "GameCollectionViewCell"
        static let LanguagesPairTableViewCellIdentifier = "LanguagesPairTableViewCellIdentifier"
	}
	
}