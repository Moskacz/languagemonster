//
//  LanguagesPairsViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 28.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class LanguagesPairsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: LanguagesPairsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        viewModel.fetchData {
            self.tableView.reloadData()
        }
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
    }
}

extension LanguagesPairsViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfLanguagesPairs
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.CellIdentifiers.LanguagesPairTableViewCellIdentifier) as! LanguagesPairTableViewCell
        let cellViewModel = viewModel.languagesPairCellViewModelForRow(indexPath.row)
        cell.configureWithViewModel(cellViewModel)
        return cell
    }
}

extension LanguagesPairsViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let clickedCellViewModel = viewModel.languagesPairCellViewModelForRow(indexPath.row)
        print(clickedCellViewModel.mainText)
    }
}
