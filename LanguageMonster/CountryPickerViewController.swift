//
//  CountryPickerViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 19.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

protocol CountryPickerViewControllerDelegate: class {
	func countryPickerDidSelectCountry(country: CountryDTO)
}


class CountryPickerViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	var viewModel: CountryPickerViewModel!
	weak var delegate: CountryPickerViewControllerDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()
        setupTableView()
        viewModel.fetchCountries {
            self.tableView.reloadData()
        }
	}
	
	@IBAction func cancelButtonTapped(sender: UIBarButtonItem) {
		dismissViewControllerAnimated(true, completion: nil)
	}
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
    }
	
}

extension CountryPickerViewController: UITableViewDataSource, UITableViewDelegate {
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return viewModel.sectionsCount()
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.itemsCountForSection(section)
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("CountryTableViewCell") as! CountryTableViewCell
		let country = viewModel.countryForIndexPath(indexPath)!
		cell.countryName.text = country.name
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if let country = viewModel.countryForIndexPath(indexPath) {
			delegate?.countryPickerDidSelectCountry(country)
		}
	}
}
