//
//  ButtonWithLabelStackView.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class CustomStackView: UIStackView {
	
	func addLabelWithText(text: String) {
		let label = getLabelWithText(text)
		addArrangedSubview(label)
	}
	
	 func removeExistingLabel() {
		if let label = subviews.filter({ $0 is UILabel }).first {
			removeArrangedSubview(label)
		}
	}
	
	// MARK: views creation
	
	private func getLabelWithText(text: String) -> UILabel {
		let label = UILabel()
		label.numberOfLines = 0
		label.text = text
		label.textAlignment = .Center
		label.backgroundColor = UIColor.whiteColor()
		label.layer.cornerRadius = 5.0
		label.clipsToBounds = true
		
		return label
	}
}

