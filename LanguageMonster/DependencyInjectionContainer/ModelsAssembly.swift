//
//  ModelsAssembly.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

struct ModelsAssembly: ObjectsAssembler {
    
    func assemblyObjectsWithContainer(container: Container) {
        container.register(UserModel.self) { resolver in
            let service = resolver.resolve(UsersService.self)!
            let userDAO = resolver.resolve(UserDAO.self)!
            return UserModel(usersService: service, userDAO: userDAO)
        }
        
        container.register(LanguagesModel.self) { resolver in
            return LanguagesModel(languagesService: resolver.resolve(LanguagesService.self)!)
        }
    }
    
}