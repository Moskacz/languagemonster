//
//  ObjectsAssembler.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

protocol ObjectsAssembler {
    func assemblyObjectsWithContainer(container: Container)
}