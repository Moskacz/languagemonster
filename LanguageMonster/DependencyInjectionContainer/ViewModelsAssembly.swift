//
//  ViewModelsAssembly.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

struct ViewModelsAssembly: ObjectsAssembler {
    
    func assemblyObjectsWithContainer(container: Container) {
        container.register(LoginViewModel.self) { resolver in
            return LoginViewModel(userModel: resolver.resolve(UserModel.self)!)
        }
        
        container.register(RegisterViewModel.self) { resolver in
            return RegisterViewModel(userModel: resolver.resolve(UserModel.self)!)
        }
        
        container.register(MenuViewModel.self) { resolver in
            return MenuViewModel(userDAO: resolver.resolve(UserDAO.self)!)
        }
        
        container.register(GamesListViewModel.self) { resolver in
            return GamesListViewModel()
        }
        
        container.register(LanguagesPairsViewModel.self) { resolver in
            return LanguagesPairsViewModel(languagesModel: resolver.resolve(LanguagesModel.self)!)
        }
        
        container.register(CountryPickerViewModel.self) { resolver in
            return CountryPickerViewModel(languagesModel: resolver.resolve(LanguagesModel.self)!)
        }
    }
}