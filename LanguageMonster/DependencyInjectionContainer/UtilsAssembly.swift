//
//  UtilsAssembly.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

struct UtilsAssembly: ObjectsAssembler {
    
    func assemblyObjectsWithContainer(container: Container) {
        
        container.register(CoreDataStack.self) { _ in
            return CoreDataStack()
        }.inObjectScope(.Hierarchy)
        
        container.register(NSUserDefaults.self) { _ in
            return NSUserDefaults.standardUserDefaults()
        }
        
        container.register(UserDAO.self) { resolver in
            return UserDAO(userDefaults: resolver.resolve(NSUserDefaults.self)!)
        }
        
    }
}