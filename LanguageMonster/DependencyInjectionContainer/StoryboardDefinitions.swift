//
//  StoryboardDefinitions.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

extension SwinjectStoryboard {
    class func setup() {
        
        ViewModelsAssembly().assemblyObjectsWithContainer(defaultContainer)
        ModelsAssembly().assemblyObjectsWithContainer(defaultContainer)
        ServicesAssembly().assemblyObjectsWithContainer(defaultContainer)
        UtilsAssembly().assemblyObjectsWithContainer(defaultContainer)
        
        defaultContainer.registerForStoryboard(LoginViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(LoginViewModel.self)
        }
        
        defaultContainer.registerForStoryboard(RegisterViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(RegisterViewModel.self)
        }
        
        defaultContainer.registerForStoryboard(MenuViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(MenuViewModel)
        }
        
        defaultContainer.registerForStoryboard(GamesListViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(GamesListViewModel.self)
        }
        
        defaultContainer.registerForStoryboard(LanguagesPairsViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(LanguagesPairsViewModel.self)
        }
        
        defaultContainer.registerForStoryboard(CountryPickerViewController.self) { resolver, object in
            object.viewModel = resolver.resolve(CountryPickerViewModel.self)
        }
    }
}