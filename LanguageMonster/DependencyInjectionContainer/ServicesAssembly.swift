//
//  ServicesAssembly.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 10.01.2016.
//  Copyright © 2016 Michal Moskala. All rights reserved.
//

import Foundation
import Swinject

struct ServicesAssembly: ObjectsAssembler {
    
    func assemblyObjectsWithContainer(container: Container) {
        container.register(HttpClient.self) { _ in
            return HttpClient()
        }
        
        container.register(UsersService.self) { resolver in
            return UsersService(httpClient: resolver.resolve(HttpClient.self)!)
        }
        
        container.register(LanguagesService.self) { resolver in
            return LanguagesService(httpClient: resolver.resolve(HttpClient.self)!)
        }
    }
    
}