//
//  MenuItem.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 28.11.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

enum MenuItem: String {
    case Profile = "Profile", Languages = "Languages", Games = "Games", Settings = "Settings", Logout = "Logout"
}