//
//  LanguagesPairDTO.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 28.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct LanguagesPairDTO {
    let baseLanguage: LanguageDTO
    let targetLanguage: LanguageDTO
    let learners: Int
}