//
//  CountryDTO.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 12/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct CountryDTO {
	let name: String
	let language: LanguageDTO
}