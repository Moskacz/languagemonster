//
//  LanguageDTO.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 07/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct LanguageDTO {
	let acronym: String
	let englishName: String
	let identifier: String
}