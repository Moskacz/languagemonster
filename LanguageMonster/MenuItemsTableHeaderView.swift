//
//  MenuItemsTableHeaderView.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class MenuItemsTableHeaderView: UIView {
	
	@IBOutlet weak var emailLabel: UILabel!
	
	func setupEmailLabelForEmail(email: String?) {
		emailLabel.text = email
	}
}
