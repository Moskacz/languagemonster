//
//  LanguagesParser.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 07/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct LanguageParser {

	func languageFromJSON(json: [String: String]) -> LanguageDTO? {
		if let acronym = json["acronym"], englishName = json["english_name"], identifier = json["id"] {
			return LanguageDTO(acronym: acronym, englishName: englishName, identifier: identifier)
		}
		return nil
	}
    
    func languagesPairFromJSON(json: [String: AnyObject]) -> LanguagesPairDTO? {
        let learners = json["learners"] as? Int
        var baseLanguage: LanguageDTO? = nil
        var targetLanguage: LanguageDTO? = nil
        
        if let baseLanguageJSON = json["base_language"] as? [String: String] {
            baseLanguage = languageFromJSON(baseLanguageJSON)
        }
        
        if let targetLanguageJSON = json["target_language"] as? [String: String] {
            targetLanguage = languageFromJSON(targetLanguageJSON)
        }
        
        if learners != nil && baseLanguage != nil && targetLanguage != nil {
            return LanguagesPairDTO(baseLanguage: baseLanguage!, targetLanguage: targetLanguage!, learners: learners!)
        }
        
        return nil
    }
	
}