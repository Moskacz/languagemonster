//
//  CountryParser.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 12/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct CountryParser {
	
	let languageParser = LanguageParser()
	
	func countryFromJSON(json: [String: AnyObject]) -> CountryDTO? {
		if let name = json["country"] as? String, languageJSON = json["language"] as? [String: String] {
			if let language = languageParser.languageFromJSON(languageJSON) {
				return CountryDTO(name: name, language: language)
			}
		}
		
		return nil
	}
	
}