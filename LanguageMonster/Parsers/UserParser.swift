//
//  UserParser.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct UserParser {
	
	func userFromJSON(json: [String: AnyObject]) -> User? {
		if let email = json["email"] as? String {
			return User(email: email)
		}
		
		return nil
	}
}