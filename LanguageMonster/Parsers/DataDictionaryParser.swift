//
//  DataDictionaryParser.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct DataDictionaryParser {
	
	func jsonDictionaryFromObject(object: AnyObject?) -> [String: AnyObject]? {
		if let json = object as? [String: AnyObject] {
			return json["data"] as? [String: AnyObject]
		}
		
		return nil
	}
}