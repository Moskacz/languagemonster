//
//  LoginViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 08.09.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var scrollView: UIScrollView!
    
    var viewModel: LoginViewModel!
	var fieldValidators: [TextFieldValidator] = []
	var scrollViewInsetCoordinator: ScrollViewKeyboardInsetCoordinator!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		scrollViewInsetCoordinator = ScrollViewKeyboardInsetCoordinator(scrollView: scrollView)
		setupValidators()
	}
	
	private func setupValidators() {
		let emailFieldValidator = TextFieldValidator(textField: emailTextField, validator: EmailFormatValidator())
		fieldValidators.append(emailFieldValidator)
		
		let passwordFieldValidator = TextFieldValidator(textField: passwordTextField, validator: TextLengthValidator(requiredTextLength: 5))
		fieldValidators.append(passwordFieldValidator)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	@IBAction func loginButtonTapped(sender: UIButton) {
		viewModel.email = emailTextField.text
		viewModel.password = passwordTextField.text
		viewModel.loginUser().subscribeNext { logged in
			if logged {
				self.moveToDashboardViewController()
			}
		}
	}
	
	private func moveToDashboardViewController() {
		performSegueWithIdentifier(Constants.SegueIdentifiers.ShowDashboardViewControllerSegue, sender: self)
		if let rootNavigationController = navigationController as? RootNavigationController {
			rootNavigationController.removeViewControllersFromStackDespiteTopViewController()
		}
	}
	
}
