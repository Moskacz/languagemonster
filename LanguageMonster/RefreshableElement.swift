//
//  RefreshableElement.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 12/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

protocol RefreshableElement: class {
	func refresh()
}