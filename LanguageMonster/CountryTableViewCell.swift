//
//  CountryTableViewCell.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
	@IBOutlet weak var countryImage: UIImageView!
	@IBOutlet weak var countryName: UILabel!
}