//
//  LanguagesPairCellViewModel.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 29.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

struct LanguagesPairCellViewModel {
    
    var mainText = ""
    
    init(languagesPair: LanguagesPairDTO) {
        computeMainTextFromPair(languagesPair)
    }
    
    private mutating func computeMainTextFromPair(pair: LanguagesPairDTO) {
        mainText = "\(pair.baseLanguage.englishName) -> \(pair.targetLanguage.englishName)"
    }
    
}