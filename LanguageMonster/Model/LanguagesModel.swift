//
//  LanguagesModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 28/10/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class LanguagesModel {
	
	let languagesService: LanguagesService
	
	init(languagesService: LanguagesService) {
		self.languagesService = languagesService
	}
	
	convenience init() {
		self.init(languagesService: LanguagesService())
	}
	
	func getAllCountries() -> Observable<[CountryDTO]> {
		return languagesService.getCountries()
	}
	
    func getLanguagesPairs() -> Observable<[LanguagesPairDTO]> {
        return languagesService.getLanguagesPairs()
    }
}
