//
//  RegisterViewModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class RegisterViewModel {
	
	let userModel: UserModel
	
	var email: String?
	var password: String?
	var repeatPassword: String?
	var country: CountryDTO?

	init(userModel: UserModel) {
		self.userModel = userModel
	}
	
	func registerUser() -> Observable<Bool> {
		if let nonNilemail = email, nonNilpassword = password, nonNilRepeatPassword = repeatPassword, nonNilCountry = country {
			let parameters = RegisterRequestParameters(email: nonNilemail, password: nonNilpassword, repeatPassword: nonNilRepeatPassword, language: nonNilCountry.language.acronym, country: nonNilCountry.name)
			return userModel.registerUserWithParameters(parameters)
		} else {
			return just(false)
		}
	}
	
}