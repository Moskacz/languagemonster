//
//  RegisterModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class UserModel {
	
	let usersService: UsersService
	let userDAO: UserDAO
	
	init(usersService: UsersService, userDAO: UserDAO) {
		self.usersService = usersService
		self.userDAO = userDAO
	}
	
	func registerUserWithParameters(parameters: RegisterRequestParameters) -> Observable<Bool> {
		return self.usersService.registerUserWithParameters(parameters)
	}
	
	func loginUserWithParameters(parameters: LoginRequestParameters) -> Observable<Bool> {
		return usersService.loginUserWithParameters(parameters).map({ (user: User?) -> Bool in
			if let validUser = user {
				self.userDAO.saveUser(validUser)
				return true
			}
			
			return false
		})
	}
	
}