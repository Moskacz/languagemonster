//
//  GamesListViewModel.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 05.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class GamesListViewModel {
    private let games: [Game]
    
    init() {
        games = [.DuckShooter]
    }
    
    var numberOfGames: Int {
        return games.count
    }
    
    func gameForIndex(index: Int) -> Game {
        return games[index]
    }
    
}