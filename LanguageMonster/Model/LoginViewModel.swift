//
//  LoginViewModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel {
	
	let userModel: UserModel
	
	var email: String?
	var password: String?
	
	init(userModel: UserModel) {
		self.userModel = userModel
	}
	
	func loginUser() -> Observable<Bool> {
		if let nonNilEmail = email, nonNilPassword = password {
			let parameters = LoginRequestParameters(email: nonNilEmail, password: nonNilPassword)
			return self.userModel.loginUserWithParameters(parameters)
		} else {
			return just(false)
		}
	}
	
}