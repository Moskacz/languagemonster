//
//  MenuViewModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class MenuViewModel {
	private let userDAO: UserDAO
    private let menuItems: [MenuItem]
	
	init(userDAO: UserDAO) {
		self.userDAO = userDAO
        menuItems = [.Profile, .Languages, .Games, .Settings, .Logout]
	}
	
	func getUserEmail() -> String? {
		return userDAO.getUser()?.email
	}
	
	var numberOfMenuItems: Int {
		return menuItems.count
	}
	
	func menuItemForRow(row: Int) -> MenuItem {
		return menuItems[row]
	}
}