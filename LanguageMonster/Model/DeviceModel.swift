//
//  DeviceModel.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 28/10/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation
import RxSwift

class DeviceModel {
	
	let devicesService: DevicesService
	let userDefaults = NSUserDefaults()
	
	// MARK: Initialization
	
	init(devicesService: DevicesService) {
		self.devicesService = devicesService
	}
	
	// MARK: retrieving
	
	func getDeviceToken() -> Observable<String> {
		if let token = userDefaults.stringForKey("deviceToken") {
			return just(token)
		} else{
			return self.devicesService.registerDevice().doOn(onNext: { token in
				self.userDefaults.setObject(token, forKey: "deviceToken")
			})
		}
	}
	
}