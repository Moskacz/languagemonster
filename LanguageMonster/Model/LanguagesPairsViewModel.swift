//
//  LanguagesPairsViewModel.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 28.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class LanguagesPairsViewModel {
    
    private let languagesModel: LanguagesModel
    private var languagesPairs = [LanguagesPairDTO]()
    
    init(languagesModel: LanguagesModel) {
        self.languagesModel = languagesModel
    }
    
    func fetchData(completion: Void -> Void) {
        languagesModel.getLanguagesPairs().subscribeNext { pairs in
            self.languagesPairs = pairs
            completion()
        }
    }
    
    var numberOfLanguagesPairs: Int {
        return languagesPairs.count
    }
    
    func languagesPairCellViewModelForRow(row: Int) -> LanguagesPairCellViewModel {
        return LanguagesPairCellViewModel(languagesPair: languagesPairs[row])
    }

}