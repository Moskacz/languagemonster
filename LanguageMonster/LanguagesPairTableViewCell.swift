//
//  LanguagesPairTableViewCell.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 29.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class LanguagesPairTableViewCell: UITableViewCell {

    func configureWithViewModel(viewModel: LanguagesPairCellViewModel) {
        textLabel?.text = viewModel.mainText
    }

}
