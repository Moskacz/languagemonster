//
//  DuckShooterScene.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 05.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import SpriteKit

class DuckShooterScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        backgroundColor = UIColor.whiteColor()
        
        let sprite = SKSpriteNode(color: UIColor.blueColor(), size: CGSize(width: 50, height: 50))
        sprite.position = CGPoint(x: 10, y: 10)
        addChild(sprite)
        
        let actionMove = SKAction.moveTo(CGPoint(x: 300, y: 300), duration: 5.0)
        let actionMoveDone = SKAction.removeFromParent()
        
        sprite.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
}