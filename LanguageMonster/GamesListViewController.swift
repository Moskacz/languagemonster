//
//  GamesListViewController.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 05.12.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class GamesListViewController: UIViewController {
    var viewModel: GamesListViewModel!
}

extension GamesListViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfGames
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellIdentifier = Constants.CellIdentifiers.GameCollectionViewCell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        cell.contentView.backgroundColor = UIColor.redColor()
        return cell
    }
}

extension GamesListViewController: UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
}




