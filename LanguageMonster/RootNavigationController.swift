//
//  RootNavigationController.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
	
	func removeViewControllersFromStackDespiteTopViewController() {
		if let viewController = topViewController {
			viewControllers = [viewController]
		}
	}
	
}
