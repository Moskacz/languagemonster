//
//  CustomButton.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 12.09.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        setupOvalCorners()
    }
    
    private func setupOvalCorners() {
        layer.cornerRadius = CGFloat(5)
    }

}
