//
//  MenuViewController.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 22/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
    func menuViewControllerDidSelectMenuItem(menuItem: MenuItem)
}

class MenuViewController: UIViewController {
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var tableHeaderView: MenuItemsTableHeaderView!
    var viewModel: MenuViewModel!
    weak var delegate: MenuViewControllerDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()
        tableHeaderView.setupEmailLabelForEmail(viewModel.getUserEmail())
        tableView.tableFooterView = UIView()
	}
    
}

extension MenuViewController: UITableViewDataSource {
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.numberOfMenuItems
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(Constants.CellIdentifiers.MenuItemTableViewCellIdentifier)!
		let menuItem = viewModel.menuItemForRow(indexPath.row)
		cell.textLabel?.text = menuItem.rawValue
		return cell
	}
}

extension MenuViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuItem = viewModel.menuItemForRow(indexPath.row)
        delegate?.menuViewControllerDidSelectMenuItem(menuItem)
    }
}
