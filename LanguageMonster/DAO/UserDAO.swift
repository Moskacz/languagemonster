//
//  UserDAO.swift
//  LanguageMonster
//
//  Created by Michal Moskala on 21/11/15.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

class UserDAO {
	let emailKey = "kUserEmail"
	let userDefaults: NSUserDefaults
	
	init(userDefaults: NSUserDefaults) {
		self.userDefaults = userDefaults
	}
	
	func getUser() -> User? {
		if let email = userDefaults.stringForKey(emailKey) {
			return User(email: email)
		}
		
		return nil
	}
	
	func saveUser(user: User) {
		userDefaults.setObject(user.email, forKey: emailKey)
	}
}