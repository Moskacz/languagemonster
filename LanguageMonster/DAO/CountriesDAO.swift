//
//  File.swift
//  LanguageMonster
//
//  Created by Michał Moskała on 19.10.2015.
//  Copyright © 2015 Michal Moskala. All rights reserved.
//

import Foundation

protocol CountriesDAO {
    var countriesCount: Int { get }
}